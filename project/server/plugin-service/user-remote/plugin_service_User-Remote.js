
'use strict';

const GuidGenerator = require('z-abs-corelayer-cs/guid-generator');
const PluginService = require('z-abs-corelayer-server/plugin-service');


class UserRemote extends PluginService {
  constructor() {
    super();
    this.tokens = new Map();
    this.authorizations = ['Guest', 'Reporter', 'Developer', 'Maintainer', 'Owner'];
  }
  
  onInit() {
    this.done();
  }
  
  onExit() {
    this.done();
  }
    
  getUser(token) {
    return this.tokens.get(token);
  }
  
  login(userName, authorization) {
    const token = GuidGenerator.create();
    this.tokens.set(token, {
      userName: userName,
      authorization: authorization
    });
    return token;
  }
  
  logout(token) {
    this.tokens.delete(token);
  }
  
  getAuthorization(token) {
    const user = this.tokens.get(token);
    if(user) {
      return user.authorization;
    }
    else {
      return null;
    }
  }
}


module.exports = UserRemote;
